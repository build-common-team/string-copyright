use Test2::V0;

use Test::RDF::DOAP::Version;

plan 1;

doap_version_ok( 'String-Copyright', 'String::Copyright' );

done_testing;
